from rest_framework import serializers
from . import models

class SystemUsersSerializer(serializers.Serializer):
    UserId = serializers.CharField()
    email = serializers.CharField()
    username = serializers.CharField()
    name = serializers.CharField()


