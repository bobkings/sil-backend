import re
import requests
import datetime
from rest_framework import serializers
from django.contrib.auth import get_user_model
from dateutil import parser
from django.contrib.auth.models import Group
from django.conf import settings
from user_manager import models as models
# from timeline import models as timeline_models
# from subscription import models as subscription_models
from django.db.models import Q


class UserDetailSerializer(serializers.Serializer):
    username = serializers.CharField()
    id_number = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()


class SystemUsersSerializer(serializers.Serializer):
    UserId = serializers.CharField()
    email = serializers.CharField()
    firstname = serializers.CharField()
    lastname = serializers.CharField()


class PasswordChangeSerializer(serializers.Serializer):
    new_password = serializers.CharField()
    confirm_password = serializers.CharField()
    current_password = serializers.CharField()

class UserPasswordChangeSerializer(serializers.Serializer):
    otp = serializers.CharField()
    email = serializers.CharField()
    password = serializers.CharField()
    confirm_password = serializers.CharField()

class GroupSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()

class UserIdSerializer(serializers.Serializer):
    user_id = serializers.CharField()

class UserEmailSerializer(serializers.Serializer):
    email = serializers.CharField()
    serverurl = serializers.CharField()


class DepartmentSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()
    keyword = serializers.CharField()


class UsersSerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    email = serializers.CharField()
    name = serializers.CharField()
    username = serializers.CharField()
    bio = serializers.CharField()
    is_active = serializers.CharField()
    is_suspended = serializers.CharField()
    last_login = serializers.DateTimeField()
    user_groups = serializers.SerializerMethodField(read_only=True)

    def get_user_groups(self, obj):
        current_user = obj
        allgroups = Group.objects.filter(user=current_user)
        serializer = GroupSerializer(allgroups, many=True)
        return serializer.data

    class Meta:
        model = get_user_model()
        fields = [
            'id', 'email', 'name', 'username', 'bio', 'is_active', 'is_suspended','user_groups','date_created','last_login'
        ]
 
class SwapUserDepartmentSerializer(serializers.Serializer):
    user_id = serializers.CharField()
    department_id = serializers.CharField()


class RoleSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()



class CreateUserSerializer(serializers.Serializer):
    email = serializers.CharField()
    name = serializers.CharField()
    username = serializers.CharField()
    password = serializers.CharField()

class AddUserSerializer(serializers.Serializer):
    email = serializers.CharField()
    name = serializers.CharField()
    usernamename = serializers.CharField()
    role_name = serializers.CharField()

class CreateProfileSerializer(serializers.Serializer):
    name = serializers.CharField()
    username = serializers.CharField()
    bio = serializers.CharField(allow_blank=True, allow_null=True)


class SuspendUserSerializer(serializers.Serializer):
    user_id = serializers.CharField()
    remarks = serializers.CharField()

class AccountActivitySerializer(serializers.Serializer):
    id = serializers.CharField()
    activity = serializers.CharField()

class OtpSerializer(serializers.Serializer):
    otp = serializers.CharField()
    email = serializers.CharField()

class ResendOtpSerializer(serializers.Serializer):
    email = serializers.CharField()


class AccountActivityDetailSerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    document = serializers.SerializerMethodField(read_only=True)
    user = serializers.SerializerMethodField(read_only=True)
    document_status= serializers.CharField()
    action_time = serializers.DateTimeField()

class EditUserSerializer(serializers.Serializer):
    name = serializers.CharField()
    email = serializers.CharField()
    username = serializers.CharField()
    account_id = serializers.CharField()


class ManageRoleSerializer(serializers.Serializer):
    role_id = serializers.ListField(required=True)
    account_id = serializers.CharField()